# AutonHome

Domotic application who allows to handle your connected devices.
Realized in swift and include a management with an app on phone and on iWatch.

* Domotic
* Management of devices of mutliples houses
* Realized in Swift 3
* Include iWatch management

## Purpose

Project realized in the context of school.

The aim of this project was to learn the different way to managed the domotic with iOS.

## Technologies

* Swift 3

//
//  InterfaceController.swift
//  AutonHome WatchKit Extension
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import WatchKit
import WatchConnectivity
import UserNotifications

class InterfaceController: WKInterfaceController {

    @IBOutlet weak var tableRow: WKInterfaceTable!
    @IBOutlet weak var primaryHomeLabel: WKInterfaceLabel!
    
    var homeManager = HMHomeManager()
    var homes = [HMHome]()
    
    var dictionaryAccessory : [String : AccessoryWatch] = [:]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        self.homeManager.delegate = self
        if (getHomes()) {
            self.primaryHomeLabel.setText(homes[0].name)
        }
    }
    
    func getHomes () -> Bool {
        homeManager.delegate = self
        let numberOfHomes = homeManager.homes.count
        if (numberOfHomes > 0) {
            homes = homeManager.homes
            return true
        }
        return false
    }
    
    func setupTable() {
        let res = dictionaryAccessory.map {$0}
        print (res)
        tableRow.setNumberOfRows(dictionaryAccessory.count, withRowType: "AccessoryRow")
        for (index, accessory) in dictionaryAccessory.enumerated() {
            if let row = tableRow.rowController(at: index) as? AccessoryRow {
                row.accessoryLabel.setText(accessory.value.name)
            }
        }
        
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "ObjectController", context: [
            "id" : Array(dictionaryAccessory.keys)[rowIndex],
            "name" : Array(dictionaryAccessory.values)[rowIndex].name!,
            "status" : Array(dictionaryAccessory.values)[rowIndex].status!
        ])
    }

}

extension InterfaceController : WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("watch OK \(activationState)")
    }

    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
        print(userInfo)
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print(message)

        if let valueStartReceived = message["primaryHome"] as? String {
            DispatchQueue.main.async {
                self.primaryHomeLabel.setText(valueStartReceived)
            }
        }

        for msg in message {
            if (msg.key != "primaryHome") {
                if (msg.key.hasSuffix("status")) {
                    let id = msg.key.split(separator: "_")[0]
                    if (dictionaryAccessory.keys.contains(String(id))) {
                        dictionaryAccessory[String(id)]!.status = (msg.value as! Bool)
                    } else {
                        dictionaryAccessory[String(id)] = AccessoryWatch(uuid: msg.key, status: msg.value as! Bool)
                    }
                } else {
                    if (dictionaryAccessory.keys.contains(msg.key)) {
                        dictionaryAccessory[msg.key]!.name = (msg.value as! String)
                    } else {
                        dictionaryAccessory[msg.key] = AccessoryWatch(uuid: msg.key, name: msg.value as! String)
                    }
                }
            }
        }

        self.setupTable()

    }

}

extension InterfaceController : HMHomeManagerDelegate {
    func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {

    }
    
    func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {

    }
    
    func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
    }
}

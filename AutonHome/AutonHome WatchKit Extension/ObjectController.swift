//
//  ObjectController.swift
//  AutonHome WatchKit Extension
//
//  Created by Bérangère La Touche on 17/01/2019.
//  Copyright © 2019 Bérangère La Touche. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ObjectController: WKInterfaceController {

    @IBOutlet weak var accessoryName: WKInterfaceLabel!
    @IBOutlet weak var activationSwitch: WKInterfaceSwitch!
    
    var objectId : String!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        let data = context as! [String : Any]
        self.objectId = data["id"] as? String
        self.accessoryName.setText(data["name"] as? String)
        self.activationSwitch.setOn(data["status"] as! Bool)
    }
    
    @IBAction func objectStatus(_ value: Bool) {
        let session = WCSession.default
        
        guard session.isReachable else {
            return
        }
        
        session.sendMessage([
            "objectId": self.objectId,
            "status" : value
        ], replyHandler: nil, errorHandler: {(err) in
            print(err)
        })
    }
}

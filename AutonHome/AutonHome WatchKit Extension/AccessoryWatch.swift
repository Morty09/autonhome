//
//  File.swift
//  AutonHome WatchKit Extension
//
//  Created by Bérangère La Touche on 22/01/2019.
//  Copyright © 2019 Bérangère La Touche. All rights reserved.
//

class AccessoryWatch {
    
    var uuid : String
    var name : String?
    var status : Bool?
    
    init(uuid : String, name : String = "", status : Bool = false) {
        self.uuid = uuid
        self.name = name
        self.status = status
    }
    
}

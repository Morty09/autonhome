//
//  Colors.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 26/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static var taupeColor: UIColor  { return UIColor(red:0.37, green:0.36, blue:0.36, alpha:1.0) }
    static var greenColor: UIColor  { return UIColor(red:0.37, green:0.55, blue:0.15, alpha:1.0) }
    static let redColor = UIColor(red:0.83, green:0.30, blue:0.16, alpha:1.0)
}

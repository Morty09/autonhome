//
//  UtilsHomeKit.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 18/01/2019.
//  Copyright © 2019 Bérangère La Touche. All rights reserved.
//

import UIKit
import HomeKit

class UtilsHomeKit {
    
    static let shared = UtilsHomeKit()
    
    var homeManager = HMHomeManager()
    
    init() {}
    
//    func setHomeManager(homeManager: HMHomeManager) {
//        self.homeManager = homeManager
//    }
    
    func getHomeManager() -> HMHomeManager {
        return self.homeManager
    }
    
    func allowNotifications(hmService : String, hmCharacteristic : String, hmAllServices : [HMService], completionHandler: @escaping (_ result: HMCharacteristic)->()) {
        for service in hmAllServices {
            for characteristic in service.characteristics {
                if (hmCharacteristic == characteristic.characteristicType) {
                    if (service.serviceType == hmService) {
                        characteristic.enableNotification(true) { error in
                            if error != nil {
                                print("Something went wrong when enabling notification for a characteristic.")
                            }
                        }
                        completionHandler(characteristic)
                        return
                    }
                }
             }
        }
    }
    
    func updateStatement(value: Any, hmService : String, hmCharacteristic : String, hmAllServices : [HMService]) {
        for service in hmAllServices {
            for characteristic in service.characteristics {
                if (hmCharacteristic == characteristic.characteristicType) {
                    if (service.serviceType == hmService) {
                        characteristic.writeValue(value) { error in
                            if error != nil {
                                print("Something went wrong when attempting to update the service characteristic.")
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
}

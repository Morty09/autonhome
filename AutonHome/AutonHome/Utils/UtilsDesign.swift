//
//  UtilsDesign.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 27/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import Foundation
import UIKit

class UtilsDesign {
    
    func borderBottom(view: UIView) {
        let border = CALayer()
        let width = CGFloat(1.0)
        
        border.borderColor = Colors.taupeColor.cgColor
        border.frame = CGRect(x: 0, y: view.frame.size.height - width, width: view.frame.size.width, height: view.frame.size.height)
        border.borderWidth = width
        
        view.layer.addSublayer(border)
        view.layer.masksToBounds = true
    }
    
    func shadowView(view: UIView, shadowRadius: CGFloat? = 2.0) {
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = shadowRadius!
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 2.0
    }
    
    func roundedView(view: UIView) {
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }
    
    func checkboxDesign(view: UIView) {
        view.layer.borderWidth = 1.0
        view.layer.borderColor = Colors.taupeColor.cgColor
        view.layer.cornerRadius = 2.0
    }
    
}

//
//  UtilsAlert.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 22/01/2019.
//  Copyright © 2019 Bérangère La Touche. All rights reserved.
//

import UIKit

class UtilsAlert {
    
    func setCustomAlertWithMessage(title : String, message : String, view : UIViewController) {
        let customAlertWithMessage = CustomAlertWithMessage()
        customAlertWithMessage.titleAlert = title
        customAlertWithMessage.descriptionAlert = message
        customAlertWithMessage.providesPresentationContextTransitionStyle = true
        customAlertWithMessage.definesPresentationContext = true
        customAlertWithMessage.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlertWithMessage.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlertWithMessage.delegate = view as? CustomAlertWithMessageDelegate
        view.present(customAlertWithMessage, animated: true, completion: nil)
    }
    
    func setCustomAlert(title : String, view : UIViewController) {
        let customAlert = CustomAlertViewController()
        customAlert.titleAlert = title
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.delegate = view as? CustomAlertViewDelegate
        view.present(customAlert, animated: true, completion: nil)
    }
    
}

//
//  CustomAlertViewDelegate.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 28/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

protocol CustomAlertViewDelegate: class {
    func validateButtonTapped(textFieldValue: String)
    func cancelButtonTapped()
}

//
//  CustomAlertControllerViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 27/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var zoneTextField: UITextField!
    @IBOutlet weak var alertTitle: UILabel!
    
    var delegate : CustomAlertViewDelegate?
    var titleAlert : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyDesign()
        self.alertTitle.text = titleAlert
        self.zoneTextField.becomeFirstResponder()
        self.view.backgroundColor = Colors.taupeColor.withAlphaComponent(0.4) 
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    func applyDesign() {
        let utils = UtilsDesign()
        utils.borderBottom(view: zoneTextField)
        utils.shadowView(view: alertView)
    }

    @IBAction func touchValidateAlert(_ sender: Any) {
        self.zoneTextField.resignFirstResponder()
        delegate?.validateButtonTapped(textFieldValue: self.zoneTextField.text!)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func touchCancelAlert(_ sender: Any) {
        self.zoneTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        self.dismiss(animated: false, completion: nil)
    }
}

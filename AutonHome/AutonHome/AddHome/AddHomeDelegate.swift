//
//  AddHomeDelegate.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 29/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

protocol AddHomeDelegate: class {
    func validateTapped()
}

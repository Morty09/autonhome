//
//  AddHomeViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import HomeKit

class AddHomeViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var imgCheckbox: UIImageView!
    
    let homeManager = HMHomeManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        applyDesign()
        self.homeManager.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(touchCheckbox))
        self.checkboxView.addGestureRecognizer(gesture)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeDown.direction = .down
         
        view.addGestureRecognizer(swipeDown)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        if gesture.direction == UISwipeGestureRecognizer.Direction.down {
            dismissKeyboard()
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func touchCheckbox() {
        if (self.imgCheckbox.isHidden == true) {
            self.imgCheckbox.isHidden = false
        } else {
            self.imgCheckbox.isHidden = true
        }
    }
    
    func applyDesign() {
        let utils = UtilsDesign()
        utils.borderBottom(view: nameTextField)
        utils.checkboxDesign(view: checkboxView)
    }

    @IBAction func touchValidateButton(_ sender: Any) {
        self.homeManager.addHome(withName: self.nameTextField.text!) { (home, err) in
            if (self.imgCheckbox.isHidden == false) {
                self.updatePrimaryHomeManager(newPrimaryHome: home!)
            }
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func updatePrimaryHomeManager(newPrimaryHome: HMHome) {
        self.homeManager.updatePrimaryHome(newPrimaryHome) { (err) in
            if (err != nil) {
                print("[ERROR] Add Home Controller - Primary home wasn't updated : \(err.debugDescription)")
            }
        }
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension AddHomeViewController : HMHomeManagerDelegate {
    func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
    }
    
    func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
    }
    
    func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
    }

}

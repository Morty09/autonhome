//
//  ObjectViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import DropDown
import HomeKit

class CreateObjectViewController: UIViewController {
    
    @IBOutlet weak var objectNameTextField: UITextField!
    @IBOutlet weak var objectTypeLabel: UILabel!
    @IBOutlet weak var roomLabel: UILabel!
    @IBOutlet weak var objectTypeView: UIView!
    @IBOutlet weak var roomView: UIView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var downArrowAccessories: UIImageView!
    
    let browserAccessories = HMAccessoryBrowser()
    let dropDownObject = DropDown()
    let dropDownRoom = DropDown()
    let homeManager = HMHomeManager()
    
    var home : HMHome!
    var accessories : [HMAccessory] = []
    var rooms : [HMRoom] = []
    var selectedAccessory : HMAccessory!
    var selectedRoom : HMRoom!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeManager.delegate = self
        browserAccessories.delegate = self
        applyDesign()
        getDetectedAccessories()
        setUpDropDownZone()
    }
    
    func getDetectedAccessories() {
        accessories.removeAll()
        browserAccessories.startSearchingForNewAccessories()
        loader.startAnimating()
        perform(#selector(stopDiscoveringAccessories), with: nil, afterDelay: 3)
    }
    
    @objc func stopDiscoveringAccessories() {
        loader.stopAnimating()
        loader.isHidden = true
        downArrowAccessories.isHidden = false
        if accessories.isEmpty {
            UtilsAlert().setCustomAlertWithMessage(title: "Aucun accessoires", message: "Aucun accessoire détectés", view: self)
        } else {
            setUpDropDownObject()
        }
    }
    
    func setUpDropDownObject() {
        let touchEvent = UITapGestureRecognizer(target: self, action:  #selector (self.showDropDownObject (_:)))
        self.objectTypeView.addGestureRecognizer(touchEvent)
        dropDownObject.anchorView = objectTypeView
        dropDownObject.textColor = Colors.taupeColor
        dropDownObject.dataSource = getNamesOfAccessories()
        
        dropDownObject.selectionAction = { (index: Int, item: String) in
            self.objectTypeLabel.text = item
            self.selectedAccessory = self.accessories[index]
        }
    }
    
    @objc func showDropDownObject(_ sender:UITapGestureRecognizer) {
        dropDownObject.show()
    }
    
    func getNamesOfAccessories() -> [String] {
        var names : [String] = []
        for obj in accessories {
            names.append(obj.name)
        }
        return names
    }
    
    func setUpDropDownZone() {
        let touchEvent = UITapGestureRecognizer(target: self, action:  #selector (self.showDropDownZone (_:)))
        self.roomView.addGestureRecognizer(touchEvent)
        self.rooms = home.rooms
        dropDownRoom.anchorView = roomView
        dropDownRoom.textColor = Colors.taupeColor
        dropDownRoom.dataSource = getNamesOfZones()
        
        dropDownRoom.selectionAction = { (index: Int, item: String) in
            self.roomLabel.text = item
            self.selectedRoom = self.rooms[index]
        }
    }
    
    @objc func showDropDownZone(_ sender:UITapGestureRecognizer) {
        dropDownRoom.show()
    }
    
    func getNamesOfZones() -> [String] {
        var names : [String] = []
        if (home.rooms.count == 0) {
            return ["Room par défaut"]
        }
        for obj in rooms {
            names.append(obj.name)
        }
        return names
    }
    
    func applyDesign() {
        
        downArrowAccessories.isHidden = true
        
        let utils = UtilsDesign()
        utils.borderBottom(view: objectNameTextField)
        utils.borderBottom(view: objectTypeView)
        utils.borderBottom(view: roomView)
        
    }
    
    @IBAction func touchValidateButton(_ sender: Any) {
        self.addAccessoryToHome()
    }
    
    func updateAccessoryName() {
        guard let name = self.objectNameTextField.text else {
            return
        }
        
        self.selectedAccessory.updateName(name) { (err) in
            if (err != nil) {
                print("[ERROR] Create Object Controller - Error when accessory name try to be updated : \(err.debugDescription)")
            }
            self.addAccessoryToRoom()
        }
    }
    
    func addAccessoryToHome() {
        guard let accessory = self.selectedAccessory else {
            return
        }
        
        self.home.addAccessory(accessory) { (err) in
            if (err != nil) {
                print("[ERROR] Create Object Controller - Error when accessory try to be added \(err.debugDescription)")
            }
            self.updateAccessoryName()
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func addAccessoryToRoom() {
        guard
            let accessory = self.selectedAccessory,
            let room = self.selectedRoom else {
            return
        }
        
        self.home.assignAccessory(accessory, to: room) { (err) in
            if (err != nil) {
                print("[ERROR] Create Object Controller - Error when assign accessory \(accessory.name) to room \(room.name)")
            }
        }
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension CreateObjectViewController : HMAccessoryBrowserDelegate {
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        accessories.append(accessory)
    }
}

extension CreateObjectViewController : HMHomeManagerDelegate {}

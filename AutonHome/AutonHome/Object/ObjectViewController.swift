//
//  ObjectViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import DropDown
import HomeKit
import SwiftHUEColorPicker
import UserNotifications

class ObjectViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var detailsObjectView: UIView!
    @IBOutlet weak var nameObjectLabel: UILabel!
    @IBOutlet weak var roomObjectLabel: UILabel!
    @IBOutlet weak var numberObjectLabel: UILabel!
    @IBOutlet weak var activationSwitch: UISwitch!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var controlViewLightBulb: UIView!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var colorPicker: SwiftHUEColorPicker!
    @IBOutlet weak var colorPreview: UIView!
    @IBOutlet weak var temperatureValue: UITextField!
    @IBOutlet weak var controlViewHeater: UIView!
    @IBOutlet weak var programPickerView: UIPickerView!
    @IBOutlet weak var programPickerView2: UIPickerView!
    
    var primaryHome : HMHome!
    var currentObject : HMAccessory!
    var currentCategory : String!
    var programs = ["Allumer", "Éteindre"]
    var programsChoosen : String!
    
    let userNotificationCenter = UNUserNotificationCenter.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userNotificationCenter.delegate = self
        self.currentObject.delegate = self
        self.programPickerView.delegate = self
        self.programPickerView.dataSource = self
        self.programPickerView2.delegate = self
        self.programPickerView2.dataSource = self
        
        applyDesign()
        setCurrentObject()
        checkCategory()
        
        colorPicker.direction = SwiftHUEColorPicker.PickerDirection.horizontal
        colorPicker.type = SwiftHUEColorPicker.PickerType.color
        colorPicker.delegate = self
        UtilsDesign().checkboxDesign(view: colorPreview)
    }
    
    func setCurrentObject() {
        self.nameObjectLabel.text = currentObject.name
        self.roomObjectLabel.text = "Room: \(currentObject.room?.name ?? "Pas de room")"
        self.numberObjectLabel.text = "Modèle: \(currentObject.model ?? "Inconnu")"
        self.programsChoosen = self.programs[0]
    }
    
    func checkCategory() {
        if (self.currentCategory == HMAccessoryCategoryTypeLightbulb) {
            launchLightBulbView()
        } else if (self.currentCategory == HMAccessoryCategoryTypeThermostat) {
            launchHeaterView()
        } else if (self.currentCategory == HMAccessoryCategoryTypeDoorLock) {
            self.controlViewHeater.isHidden = true
            self.controlViewLightBulb.isHidden = true
            allowNotificationForLocker()
        }
    }
    
    @IBAction func setTimer(_ sender: UIDatePicker) {
        let picker = sender
        picker.datePickerMode = .time
        
        let date = picker.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        sendNotification(dateComponent: components)
    }
    
    func sendNotification(dateComponent : DateComponents) {
        let content = UNMutableNotificationContent()
        content.title = "AutonHome"
        content.subtitle = "Information sur votre programme"
        content.body = "Votre \(self.currentObject.name) va s'\(self.programsChoosen ?? "no value")"
        content.categoryIdentifier = "NOTIFICATION"
        content.sound = UNNotificationSound.default
        
        
        var status : Bool
        if (self.programsChoosen == "Allumer") {
            status = true
        } else {
            status = false
        }
        content.userInfo = ["category" : self.currentCategory,
                            "status" : status]
        
        let calendarTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: true)
        let request = UNNotificationRequest(identifier: "Programme", content: content, trigger: calendarTrigger)
        
        userNotificationCenter.add(request) { (error) in
            if let error = error
            {
                print(error.localizedDescription)
            }
        }
        
    }

    func launchLightBulbView() {
        self.controlViewHeater.isHidden = true
        self.controlViewLightBulb.isHidden = false
        
        allowNotificationForLightBulb()
    }
    
    func launchHeaterView() {
        self.controlViewLightBulb.isHidden = true
        self.controlViewHeater.isHidden = false
        
        allowNotificationForHeater()
    }
    
    func allowNotificationForHeater() {
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeSwitch, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services) { (res) in
            self.setColorOfIndicator(value: res.value as! Bool)
        }
        
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeThermostat, hmCharacteristic: HMCharacteristicTypeTargetTemperature, hmAllServices: self.currentObject.services) { (res) in
            let value = res.value as! Int
            self.temperatureValue.text = ("\(value)")
        }
    }
    
    func allowNotificationForLightBulb() {
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services) { (res) in
            if (res.isNotificationEnabled == false) {
                UtilsAlert().setCustomAlertWithMessage(title: "HomeKit", message: "Lancer HomeKit", view: self)
            }
            self.setColorOfIndicator(value: res.value as! Bool)
        }
        
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeBrightness, hmAllServices: self.currentObject.services) { (res) in
            if (res.isNotificationEnabled == false) {
                UtilsAlert().setCustomAlertWithMessage(title: "HomeKit", message: "Lancer HomeKit", view: self)
            }
            self.setBrightnessValue(value: res.value as! Float)
        }
        
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeSaturation, hmAllServices: self.currentObject.services) { (resSaturation) in
            UtilsHomeKit().allowNotifications(hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeHue, hmAllServices: self.currentObject.services) { (res) in
                self.setHueValue(color: res.value as! Int, saturation: resSaturation.value as! Float)
            }
        }
    }
    
    func allowNotificationForLocker() {
        UtilsHomeKit().allowNotifications(hmService: HMServiceTypeLockMechanism, hmCharacteristic: HMCharacteristicTypeTargetLockMechanismState, hmAllServices: self.currentObject.services) { (res) in
            self.setColorOfIndicator(value: res.value as! Bool)
        }
    }
    
    func setColorOfIndicator(value: Bool) {
        if (value == true) {
            self.activationSwitch.isOn = true
            self.indicatorView.backgroundColor = Colors.greenColor
        } else {
            self.activationSwitch.isOn = false
            self.indicatorView.backgroundColor = Colors.redColor
        }
    }
    
    @IBAction func cancelColorPicker(_ sender: Any) {
        colorPreview.backgroundColor = UIColor.white
        UtilsHomeKit().updateStatement(value: Int(0), hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeSaturation, hmAllServices: self.currentObject.services)
    }
    
    @IBAction func decreaseTemperature(_ sender: Any) {
        let newTemp = Int(self.temperatureValue.text!)! - 1 < 10 ? 10 : Int(self.temperatureValue.text!)! - 1
        self.temperatureValue.text = String(newTemp)
        updateHeaterStatement(value: newTemp)
        
    }
    
    @IBAction func increaseTemperature(_ sender: Any) {
        let newTemp = Int(self.temperatureValue.text!)! + 1 > 38 ? 38 : Int(self.temperatureValue.text!)! + 1
        self.temperatureValue.text = String(newTemp)
        updateHeaterStatement(value: newTemp)
    }
    
    func updateHeaterStatement(value: Int) {
        UtilsHomeKit().updateStatement(value: value, hmService: HMServiceTypeThermostat, hmCharacteristic: HMCharacteristicTypeTargetTemperature, hmAllServices: self.currentObject.services)
    }
    
    func setBrightnessValue(value: Float) {
        self.sliderBrightness.value = value
    }

    func setHueValue(color: Int, saturation: Float) {
        if (saturation == 0) {
            self.colorPicker.currentColor = UIColor.white
            return
        }
        let c = UIColor(hue: CGFloat(color)/360.0, saturation: CGFloat(saturation), brightness: 1, alpha: 1.0)
        self.colorPicker.currentColor = c
        self.colorPreview.backgroundColor = c
    }
    
    @IBAction func updateState(_ sender: UISwitch) {
        setColorOfIndicator(value: sender.isOn)
        
        if (self.currentCategory == HMAccessoryCategoryTypeLightbulb) {
            UtilsHomeKit().updateStatement(value: sender.isOn, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services)
        } else if (self.currentCategory == HMAccessoryCategoryTypeThermostat) {
            UtilsHomeKit().updateStatement(value: sender.isOn, hmService: HMServiceTypeSwitch, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services)
        } else if (self.currentCategory == HMAccessoryCategoryTypeDoorLock) {
            UtilsHomeKit().updateStatement(value: sender.isOn, hmService: HMServiceTypeLockMechanism, hmCharacteristic: HMCharacteristicTypeTargetLockMechanismState, hmAllServices: self.currentObject.services)
            if (sender.isOn) { callMessage() }
        }
    }
    
    func onLock(value : Bool) {
        if (value) {
            for accessory in primaryHome.accessories {
                if (accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb) {
                    UtilsHomeKit().updateStatement(value: false, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services)
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeThermostat) {
                    UtilsHomeKit().updateStatement(value: 15, hmService: HMServiceTypeThermostat, hmCharacteristic: HMCharacteristicTypeTargetTemperature, hmAllServices: accessory.services)
                }
            }
        }
    }
    
    func callMessage() {
        UtilsAlert().setCustomAlertWithMessage(title: "Vous partez ?", message: "Vous êtes sûr ?", view: self)
    }

    @IBAction func updateBrightness(_ sender: UISlider) {
        UtilsHomeKit().updateStatement(value: Int(sender.value), hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeBrightness, hmAllServices: self.currentObject.services)
    }
    
    func applyDesign() {
        
        let utils = UtilsDesign()
        utils.borderBottom(view: topView)
        utils.roundedView(view: roundedView)
        utils.shadowView(view: detailsObjectView)
        utils.shadowView(view: controlViewLightBulb)
        utils.shadowView(view: controlViewHeater)
        utils.roundedView(view: indicatorView)
        
    }
    
    func convertUIColorToHSL(color: UIColor) -> (hue: CGFloat, saturation : CGFloat, brightness : CGFloat, alpha : CGFloat) {
        var hue : CGFloat = 0
        var saturation : CGFloat = 0
        var brightness : CGFloat = 0
        var alpha : CGFloat = 0
        
        color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return (hue, saturation, brightness, alpha)
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension ObjectViewController: HMAccessoryDelegate {
    func accessory(_ accessory: HMAccessory, service: HMService,
                   didUpdateValueFor characteristic: HMCharacteristic) {
        print("-------ACCESSORY CHARACTERISTIC WAS UPDATED-------")
    }
}


extension ObjectViewController: SwiftHUEColorPickerDelegate {
    func valuePicked(_ color: UIColor, type: SwiftHUEColorPicker.PickerType) {
        colorPreview.backgroundColor = color
        
        let hsl = convertUIColorToHSL(color: color)
        
        UtilsHomeKit().updateStatement(value: Int(hsl.hue*360), hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeHue, hmAllServices: self.currentObject.services)
        
        UtilsHomeKit().updateStatement(value: Int(100), hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypeSaturation, hmAllServices: self.currentObject.services)
    }
    
    
}

extension ObjectViewController : CustomAlertWithMessageDelegate {
    func okButtonTapped() {
        self.onLock(value: true)
    }
    
    func crossButtonTapped() {}
}

extension ObjectViewController: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let categoryIdentifier = response.notification.request.content.categoryIdentifier
        let objectCategory = response.notification.request.content.userInfo["category"]
        let objectStatus = response.notification.request.content.userInfo["status"] as! Bool
        
        switch categoryIdentifier
        {
        case "NOTIFICATION":
            switch response.actionIdentifier
            {
            case "decline":
                break
                
            case "accept":
                if (objectCategory as? String == HMAccessoryCategoryTypeLightbulb) {
                    UtilsHomeKit().updateStatement(value: objectStatus, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services)
                } else if (objectCategory as? String == HMAccessoryCategoryTypeThermostat) {
                    UtilsHomeKit().updateStatement(value: objectStatus, hmService: HMServiceTypeSwitch, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: self.currentObject.services)
                }
                break
            default:
                break
            }
            
        default:
            break
        }
        completionHandler()
    }
    
}

extension ObjectViewController : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.programsChoosen = programs[row]
    }
    
}

extension ObjectViewController : UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.programs.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return programs[row]
    }
}

//
//  HomeListViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import HomeKit

class HomeListViewController: UIViewController {

    @IBOutlet weak var topImageView: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var backgroundTableView: UIView!
    @IBOutlet weak var homeTableView: UITableView!
    
    var homeManager = HMHomeManager()
    var alertTextFieldValue : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyDesign()
        
        self.homeManager.delegate = self
        self.homeTableView.dataSource = self
        self.homeTableView.delegate = self
        
        checkIfHomesExist()
    }
    
    func checkIfHomesExist() {
        if (self.homeManager.homes.count == 0) {
            UtilsAlert().setCustomAlert(title: "Ajouter une Home", view: self)
        }
    }
    
    func applyDesign() {

        let utils = UtilsDesign()
        utils.borderBottom(view: topImageView)
        utils.roundedView(view: roundedView)
        utils.shadowView(view: addButton)
        utils.shadowView(view: backgroundTableView)
        
        addButton.contentMode = .center
        addButton.imageView?.contentMode = .scaleAspectFit
        
        homeTableView.separatorColor = Colors.taupeColor

    }
    
    func createHomeWhenNoHomeExisting() {
        
        if (self.alertTextFieldValue != nil) {
            self.homeManager.addHome(withName: self.alertTextFieldValue!) { (home, err) in
                if (err != nil) {
                    print("[ERROR] Home List View Controller - Home can't be added : \(err.debugDescription)")
                }
                self.homeTableView.reloadData()
            }
        }
        
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func touchAddButton(_ sender: Any) {
        let addHomeViewController = AddHomeViewController()
        self.navigationController?.pushViewController(addHomeViewController, animated: false)
    }
}

extension HomeListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeManager.homes.count
    }
    
}

extension HomeListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.textColor = Colors.taupeColor
        cell.textLabel?.text = self.homeManager.homes[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsHomeViewController = DetailsHomeViewController()
        detailsHomeViewController.currentHome = self.homeManager.homes[indexPath.row]
        self.navigationController?.pushViewController(detailsHomeViewController, animated: false)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.homeManager.removeHome(self.homeManager.homes[indexPath.row]) { (err) in
                self.checkIfHomesExist()
                self.homeTableView.reloadData()
            }
        }
    }
    
}

extension HomeListViewController : CustomAlertViewDelegate {
    
    func validateButtonTapped(textFieldValue: String) {
        self.alertTextFieldValue = textFieldValue
        self.createHomeWhenNoHomeExisting()
    }
    
    func cancelButtonTapped() {}
    
}

extension HomeListViewController : HMHomeManagerDelegate {
    
    func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
        self.homeTableView.reloadData()
    }
    
    func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
        self.homeTableView.reloadData()
    }
    
    func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
        self.homeTableView.reloadData()
    }
    
}

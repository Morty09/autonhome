//
//  DetailsHomeViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import HomeKit

class DetailsHomeViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var backgroundTableView: UIView!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var homeNameLabel: UILabel!
    @IBOutlet weak var homeAddressLabel: UILabel!
    @IBOutlet weak var roomTableView: UITableView!
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet weak var imgCheckbox: UIImageView!
    
    let homeManager = HMHomeManager()
    
    var currentHome : HMHome!
    var rooms : [HMRoom] = [] {
        didSet {
            self.roomTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.homeManager.delegate = self
        applyDesign()
        setCurrentHomeData()
        self.roomTableView.delegate = self
        self.roomTableView.dataSource = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(touchCheckbox))
        self.checkboxView.addGestureRecognizer(gesture)
    }
    
    func setCurrentHomeData() {
        self.homeNameLabel.text = self.currentHome.name
        self.rooms = self.currentHome.rooms
        if (self.currentHome.isPrimary) {
            self.imgCheckbox.isHidden = false
        }
    }
    
    @objc func touchCheckbox() {
        if (self.imgCheckbox.isHidden == true) {
            self.imgCheckbox.isHidden = false
            callModificationHomeAlert()
        } else {
            self.imgCheckbox.isHidden = true
        }
    }
    
    func applyDesign() {
        
        let utils = UtilsDesign()
        
        utils.borderBottom(view: topView)
        utils.shadowView(view: detailsView)
        utils.shadowView(view: backgroundTableView)
        utils.roundedView(view: roundedView)
        utils.checkboxDesign(view: checkboxView)
        
        roomTableView.separatorColor = Colors.taupeColor
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func touchAddZoneButton(_ sender: Any) {
        UtilsAlert().setCustomAlert(title: "Ajouter une Room", view: self)
    }
    
    func callModificationHomeAlert() {
        UtilsAlert().setCustomAlertWithMessage(title: "Home principale", message: "Confirmer ?", view: self)
    }
}

extension DetailsHomeViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            self.rooms.remove(at: indexPath.row)
            for i in self.rooms {
                print(i)
            }
        }
    }
    
}

extension DetailsHomeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = rooms[indexPath.row].name
        cell.textLabel?.textColor = Colors.taupeColor
        
        return cell
    }
    
    
}

extension DetailsHomeViewController : CustomAlertViewDelegate {
    func validateButtonTapped(textFieldValue: String) {
        self.currentHome.addRoom(withName: textFieldValue) { (room, err) in
            self.rooms.append(room!)
        }
    }
    
    func cancelButtonTapped() {}
    
}

extension DetailsHomeViewController : CustomAlertWithMessageDelegate {
    func okButtonTapped() {
        self.homeManager.updatePrimaryHome(self.currentHome) { (err) in
            if (err != nil) {
                print("[ERROR] Details Home Controller - Primary home wasn't updated : \(err.debugDescription)")
            }
        }
    }
    
    func crossButtonTapped() {
        self.imgCheckbox.isHidden = true
    }
}

extension DetailsHomeViewController : HMHomeManagerDelegate {
    func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
    }
    
    func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
    }
    
    func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
    }
}

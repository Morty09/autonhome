//
//  HomeViewController.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 24/11/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit
import HomeKit
import WatchConnectivity
import UserNotifications

class HomeViewController: UIViewController {
    
    @IBOutlet weak var topImageView: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var itemTabBar: UITabBarItem!
    @IBOutlet weak var homeNameLabel: UILabel!
    
    let homeManager = HMHomeManager()
    
    var alertTextFieldValue : String?
    var primaryHome : HMHome!
    var accessories : [HMAccessory] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        applyDesign()
        self.tabBar.delegate = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionCell")
        
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
    }
    

    func updateStatus() {
        for accessory in primaryHome.accessories {
            if (accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb) {
                UtilsHomeKit.shared.updateStatement(value: false, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.homeManager.delegate = self
        setHomeData()
    }
    
    func setHomeData() {
        self.primaryHome = self.homeManager.primaryHome
        if (self.primaryHome == nil) {
            callCreationHomeAlert()
        } else {
            self.homeNameLabel.text = self.primaryHome.name
            self.accessories = self.primaryHome.accessories
        }
        startSession()
    }
    
    func callCreationHomeAlert() {
        UtilsAlert().setCustomAlert(title: "Ajouter une Home", view: self)
    }
    
    func createHomeWhenNoHomeExisting() {
        
        if (self.alertTextFieldValue != nil) {
            self.homeManager.addHome(withName: self.alertTextFieldValue!) { (home, err) in
                self.updatePrimaryHome(home: home!) { (isUpdated) in
                    self.setHomeData()
                }
            }
        }
        
    }
    
    func startSession() {
        let session = WCSession.default
        
        guard session.isPaired,
            session.isWatchAppInstalled else {
                return
        }
        
        var accessoriesNames : [String : Any] = [:]
        for accessory in self.primaryHome.accessories {
            let uuid = accessory.uniqueIdentifier.uuidString
            accessoriesNames[uuid] = accessory.name
            accessoriesNames[uuid + "_status"] = getStateOfAccessories(id: uuid)
        }
        accessoriesNames["primaryHome"] = self.primaryHome.name
        
        session.sendMessage(accessoriesNames, replyHandler: nil, errorHandler: {(err) in
            print(err)
        })
    }
    
    @IBAction func touchSession(_ sender: Any) {
        let session = WCSession.default
        
        guard session.isPaired,
            session.isWatchAppInstalled else {
                return
        }
        
        var accessoriesNames : [String : Any] = [:]
        for accessory in self.primaryHome.accessories {
            let uuid = accessory.uniqueIdentifier.uuidString
            accessoriesNames[uuid] = accessory.name
            accessoriesNames[uuid + "_status"] = getStateOfAccessories(id: uuid)
        }
        accessoriesNames["primaryHome"] = self.primaryHome.name
        session.sendMessage(accessoriesNames, replyHandler: nil, errorHandler: {(err) in
                print(err)
        })

    }
    
    func getStateOfAccessories(id : String) -> Bool {
        var status : Bool = false
        for accessory in self.accessories {
            if (accessory.uniqueIdentifier.uuidString == id) {
                if(accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb) {
                    UtilsHomeKit.shared.allowNotifications(hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services) { (res) in
                        status = res.value as! Bool
                    }
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeThermostat) {
                    UtilsHomeKit.shared.allowNotifications(hmService: HMServiceTypeSwitch, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services) { (res) in
                        status = res.value as! Bool
                    }
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeDoorLock) {
                    UtilsHomeKit.shared.allowNotifications(hmService: HMServiceTypeLockMechanism, hmCharacteristic: HMCharacteristicTypeTargetLockMechanismState, hmAllServices: accessory.services) { (res) in
                        status = res.value as! Bool
                    }
                }
            }
        }
        return status
    }
    
    
    func updatePrimaryHome(home : HMHome, primaryHomeUpdated: @escaping (Bool?) -> Void) {
        self.homeManager.updatePrimaryHome(home) { (err) in
            if (err != nil) {
                print("[ERROR] Home View Controller - Error when updating primary home : \(err.debugDescription)")
            }
            primaryHomeUpdated(true)
        }
    }

    func applyDesign() {
        
        let utils = UtilsDesign()
        
        utils.borderBottom(view: topImageView)
        utils.roundedView(view: roundedView)
        utils.shadowView(view: addButton)
        utils.shadowView(view: tabBar, shadowRadius: 6.0)
        
        addButton.contentMode = .center
        addButton.imageView?.contentMode = .scaleAspectFit
        
        if let count = self.tabBar.items?.count {
            for i in 0...(count-1) {
                self.tabBar.items?[i].image = UIImage(named: "homeTabBar")?.withRenderingMode(.alwaysOriginal)
                self.tabBar.items?[i].imageInsets = UIEdgeInsets(top: 48, left: 0, bottom: 38, right: 0)
            }
        }

    }
    
    @IBAction func touchAddObjectButton(_ sender: Any) {
        let createObjectViewController = CreateObjectViewController()
        createObjectViewController.home = self.primaryHome
        self.navigationController?.pushViewController(createObjectViewController, animated: false)
    }
    
    func onLock(value : Bool) {
        if (value) {
            for accessory in primaryHome.accessories {
                if (accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb) {
                    UtilsHomeKit.shared.updateStatement(value: false, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services)
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeThermostat) {
                    UtilsHomeKit.shared.updateStatement(value: 15, hmService: HMServiceTypeThermostat, hmCharacteristic: HMCharacteristicTypeTargetTemperature, hmAllServices: accessory.services)
                }
            }
        }
    }
}

extension HomeViewController : HMHomeManagerDelegate {
    
    func homeManagerDidUpdateHomes(_ manager: HMHomeManager) {
    }
    
    func homeManager(_ manager: HMHomeManager, didAdd home: HMHome) {
    }
    
    func homeManager(_ manager: HMHomeManager, didRemove home: HMHome) {
    }
    
}

extension HomeViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionViewCell
        let accessory = accessories[indexPath.row]
        
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonTapped), for: UIControl.Event.touchUpInside)
        cell.deleteButton.isHidden = true
        cell.labelCell.text = accessory.name
        cell.roomCell.text = accessory.room?.name
        cell.imageViewCell.image = getBackgroundImage(accessory: accessory)
        
        return cell
        
    }
    
    @objc func deleteButtonTapped(sender : UIButton) {
        print(sender.tag)
        for acc in self.primaryHome.accessories {
            if (acc == self.accessories[sender.tag]) {
                self.homeManager.primaryHome?.removeAccessory(acc, completionHandler: { (err) in
                    if (err != nil) {
                        print("[ERROR] Home View Controller - Remove accessory failed : \(err.debugDescription)")
                    }
                })
            }
        }
        accessories.remove(at: sender.tag)
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        
        let p = gesture.location(in: self.collectionView)
        
        if let indexPath = self.collectionView.indexPathForItem(at: p) {
            let cell = self.collectionView.cellForItem(at: indexPath) as? CollectionViewCell
            cell?.deleteButton.isHidden = false
            
        } else {
            print("[ERROR] Home View Controller - Couldn't find index path for cell")
        }
    }
    
    func getBackgroundImage(accessory : HMAccessory) -> UIImage {
        
        var image : UIImage
        
        switch accessory.category.categoryType {
        case HMAccessoryCategoryTypeLightbulb:
            image = UIImage(named: "idea")!
            break
        case HMAccessoryCategoryTypeThermostat:
            image = UIImage(named: "heater")!
            break
        case HMAccessoryCategoryTypeDoorLock:
            image = UIImage(named: "door")!
            break
        default:
            image = UIImage(named: "connected")!
        }
        
        return image
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objectViewController = ObjectViewController()
        objectViewController.primaryHome = self.primaryHome
        objectViewController.currentObject = accessories[indexPath.row]
        objectViewController.currentCategory = accessories[indexPath.row].category.categoryType
        self.navigationController?.pushViewController(objectViewController, animated: false)
    }

}

extension HomeViewController : UIGestureRecognizerDelegate {
    
}

extension HomeViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accessories.count
    }
    
}

extension HomeViewController : UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let homeListViewController = HomeListViewController()
        homeListViewController.homeManager = self.homeManager
        self.navigationController?.pushViewController(homeListViewController, animated: false)
    }
    
}

extension HomeViewController : CustomAlertViewDelegate {
    
    func validateButtonTapped(textFieldValue: String) {
        self.alertTextFieldValue = textFieldValue
        DispatchQueue.main.async {
            self.createHomeWhenNoHomeExisting()
        }
    }
    
    func cancelButtonTapped() {}
    
}

extension HomeViewController: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("iphone OK \(activationState)")
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print(message)
        
        for accessory in self.primaryHome.accessories {
            let id = message["objectId"] as? String
            if (accessory.uniqueIdentifier.uuidString == id) {
                if(accessory.category.categoryType == HMAccessoryCategoryTypeLightbulb) {
                    UtilsHomeKit.shared.updateStatement(value: message["status"]!, hmService: HMServiceTypeLightbulb, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services)
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeThermostat) {
                    UtilsHomeKit.shared.updateStatement(value: message["status"]!, hmService: HMServiceTypeSwitch, hmCharacteristic: HMCharacteristicTypePowerState, hmAllServices: accessory.services)
                } else if (accessory.category.categoryType == HMAccessoryCategoryTypeDoorLock) {
                    UtilsHomeKit.shared.updateStatement(value: message["status"]!, hmService: HMServiceTypeLockMechanism, hmCharacteristic: HMCharacteristicTypeTargetLockMechanismState, hmAllServices: accessory.services)
                    onLock(value: message["status"] as! Bool)
                }
            }
        }
        self.startSession()
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}

extension HomeViewController: HMAccessoryDelegate {
    func accessory(_ accessory: HMAccessory, service: HMService,
                   didUpdateValueFor characteristic: HMCharacteristic) {
        print("-------ACCESSORY CHARACTERISTIC WAS UPDATED-------")
    }
}

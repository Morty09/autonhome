//
//  CustomAlertWithMessageDelegate.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 21/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

protocol CustomAlertWithMessageDelegate: class {
    func okButtonTapped()
    func crossButtonTapped()
}

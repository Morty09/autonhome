//
//  CustomAlertWithMessage.swift
//  AutonHome
//
//  Created by Bérangère La Touche on 21/12/2018.
//  Copyright © 2018 Bérangère La Touche. All rights reserved.
//

import UIKit

class CustomAlertWithMessage: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var alertTitle: UILabel!
    
    var delegate : CustomAlertWithMessageDelegate?
    var titleAlert : String?
    var descriptionAlert: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyDesign()
        self.alertTitle.text = titleAlert
        self.labelDescription.text = descriptionAlert
        self.view.backgroundColor = Colors.taupeColor.withAlphaComponent(0.4)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }
    
    func applyDesign() {
        let utils = UtilsDesign()
        utils.shadowView(view: alertView)
    }
    
    @IBAction func touchValidateAlert(_ sender: Any) {
        delegate?.okButtonTapped()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func touchCancelAlert(_ sender: Any) {
        delegate?.crossButtonTapped()
        self.dismiss(animated: false, completion: nil)
    }
}
